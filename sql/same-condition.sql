IF var BETWEEN 0 AND 10 THEN
  do_the_thing();
ELSIF var BETWEEN 10 AND 20 THEN
  do_the_thing(); -- Noncompliant; duplicates first condition
ELSIF var BETWEEN 20 AND 50 THEN
  do_the_another_thing();
ELSE
  do_the_rest()
END IF;